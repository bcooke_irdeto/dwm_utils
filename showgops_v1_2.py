#!/usr/bin/env python

# show_gops v1.2_20211119

import sys,subprocess


# Check command line arguments
if len(sys.argv) != 2:
	sys.stderr.write("Syntax: %s <input-media-file>\nShows frame and GOP characteristics of the Media file\n" % sys.argv[0])
	sys.exit(-1)
  
filename = sys.argv[1]

# output of command is list of strings like 'frame,0,264152,P', whereby 2nd argument is the IDR frame indication, the 3rd argument the encoded frame size and the 4th argument the frame type (I/P/B).
# coded_picture_number may be used to differentiate multiple video streams. Assuming only 1 for now.
result = [
	'frame,1,4022,Iside_data,H.26[45] User Data Unregistered SEI message',
	'frame,0,798,B',
	'frame,0,798,B,0',
	'frame,0,799,B,0',
	'frame,1,20803,I',
	'frame,0,206,B',
	'frame,0,204,B',
	'frame,0,1364,P'
]
result = subprocess.check_output(['ffprobe', filename, '-select_streams', 'v' ,'-show_frames', '-of' ,'csv', '-show_entries', 'frame=key_frame,pkt_size,pict_type']).decode("utf-8").splitlines()
frame_count = len(result)

gops = []
gop = ''	# display string

frame_count = {'I':0, 'P':0, 'B':0}
size_min    = {'I':0, 'P':0, 'B':0}
size_max    = {'I':0, 'P':0, 'B':0}
size_total  = {'I':0, 'P':0, 'B':0}

for line in result:
	if line[0:6] != "frame,": continue; # only process video frames
	video_frame_id, key_frame, pkt_size, pict_type = line.split(',', 3)

	if key_frame == '1': # start a new GOP string
		if gop != '': gops.append(gop)
		gop = ''

	pict_type = pict_type[0] # safeguard against side data
	gop += pict_type
	frame_count[pict_type] += 1
	size_min[pict_type] = int(pkt_size) if size_min[pict_type] == 0 else min(int(pkt_size), size_min[pict_type])
	size_max[pict_type] = max(int(pkt_size), size_max[pict_type])
	size_total[pict_type] += int(pkt_size)
	
if gop == '':	# there must be at least 1 frame anyway
	print("no GOPs")
	exit()

gops.append(gop) # add last one

print("number of frames    : %5d" % (sum(frame_count.values()) ) )
if sum(frame_count.values()) == 0:
	print("========= WARNING!! NO VIDEO FRAMES DETECTED!! =========" )
	exit(0)

print("number of IDR-frames: %5d (%3.2f)%%" % (len(gops), float(100* len(gops)) / sum(frame_count.values()) ) )
print("number of I-frames  : %5d (%3.2f)%%    min/avg/max size: %d/%d/%d" % (frame_count['I'], float(100* frame_count['I']) / sum(frame_count.values()), size_min['I'], 0 if frame_count['I']== 0 else size_total['I'] / frame_count['I'], size_max['I']) )
print("number of P-frames  : %5d (%3.2f)%%    min/avg/max size: %d/%d/%d" % (frame_count['P'], float(100* frame_count['P']) / sum(frame_count.values()), size_min['P'], 0 if frame_count['P']== 0 else size_total['P'] / frame_count['P'], size_max['P'] ) )
print("number of B-frames  : %5d (%3.2f)%%    min/avg/max size: %d/%d/%d" % (frame_count['B'], float(100* frame_count['B']) / sum(frame_count.values()), size_min['B'], 0 if frame_count['B']== 0 else size_total['B'] / frame_count['B'], size_max['B'] ) )
print("number of GOPs:", len(gops) )
print("min/avg/max GOP length: %d/%d/%d" % ( len(min(gops, key=len)), sum( map(len, gops) ) / len(gops), len(max(gops, key=len)) ) )

if (float(100* frame_count['B']) / sum(frame_count.values()) ) < 10.0:
	print("========= WARNING!! POST-ENCODE WM REQUIRES AT LEAST 10% NREF-B FRAMES!! =========" )

for g in gops:
	print("%3d %s" % ( len(g), g ) )
	
	




