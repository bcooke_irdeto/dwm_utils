#!/usr/bin/env python3

import argparse
import boto3
import botocore
import json
import os
import sys

import auth_token
import tenant_storage_access_info_destination

# Notes:
# 1) Before running this script, log onto AWS, either via "aws configure" or by using irdeto-aws-adfs --region <region>
#    and selecting the account for the DWM instance in question.
# 2) Any previously-set values of environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and
#    AWS_SESSION_TOKEN will cause this script to fail with an InvalidClientTokenId error, so this script will run with
#    those values unset.
# 3) This script will output export statements to set AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_SESSION_TOKEN.
#    It cannot set these variables directly, as a subprocess (this script) cannot set environment variables of its parent
#    process (the shell from which this is run). Paste these export statements into your shell to set the variables. After
#    that, any "aws s3" or other commands that require access to a customer's bucket should succeed.
# 4) Before we run the script, We need to know the following values, and will also need to set the --prod flag if this is
#    for production DWM and need to access the API with prod credentials (the auth_token library defaults to non-production):
#    - Tenant ID (-i)
#    - Bucket name (-b)
#    - Role ARN for running emedder (--dwm_role_arn)
#    - Host name of running DWM front-end (--host)

# Sample command to use this script to assume permissions to the Disney DMED Wonderland POC bucket:
#   ./assume_s3_permissions_destination.py -i 9bc851a3bd050d34904b2f9c8a26c113826ecb47 -b assetmgmt-wonderland-packages-file-poc --dwm_role_arn arn:aws:iam::712498623681:role/dwm-us-west-2-primary-wmea-ecs-instance-role --host us-west-2.prod.dwm.irdeto.io --prod
# Test access to that bucket from command line (should fail until after this script's export statements are run in
# the shell):
#   aws s3 ls s3://assetmgmt-wonderland-packages-file-poc --human-readable

dwm_role_session_name = 'wmea-session'
duration_seconds = 900  # Optional. Default = 3600

def parse_args():
    parser = argparse.ArgumentParser(description="Using tenant ID, customer's bucket name, IAM role ARN, and IAM role external ID, temporarily assume the DWM embedder's permissions to the bucket")
    parser.add_argument('--host', help='DWM front-end host name or IP (default: us-east-1.stage.dwm.irdeto.io)', default='us-east-1-stage.dwm.irdeto.io')
    parser.add_argument('--port', help='DWM front-end port number (default: 443)', default='443')
    parser.add_argument('--ignore-ssl-cert', help="Don't verify SSL certificates. NOT to be used in production.", action='store_true')
    parser.add_argument('--prod', action="store_true", default=False, help='Enable production mode, which will request auth token for prod systems. Use this when the --host parameter is for a production host.')
    required_named = parser.add_argument_group('reqiured named arguments')
    # For dev this is 9bc851a3bd050d34904b2f9c8a26c113826ecb47
    required_named.add_argument('-i', '--tenant_id', help="Customer's tenant ID number.", required=True)
    # For dev this is assetmgmt-wonderland-packages-file-poc
    required_named.add_argument('-b', '--bucket_name', help="Name of the customer's S3 storage bucket", required=True)
    # For us-west-2, this is arn:aws:iam::712498623681:role/dwm-us-west-2-primary-wmea-ecs-instance-role
    required_named.add_argument('--dwm_role_arn', help="Name of the role ARN for the watermark embedder (WMEA) that has permissions to the customer's S3 bucket", required=True)
    args = parser.parse_args()
    args.dwm_role_session_name = dwm_role_session_name
    
    return args

def main(argv):
    # Unset AWS session environment variables which, if populated, will lead to an InvalidClientTokenId error
    os.environ['AWS_ACCESS_KEY_ID'] = ""
    os.environ['AWS_SECRET_ACCESS_KEY'] = ""
    os.environ['AWS_SESSION_TOKEN'] = ""

    args = parse_args()

    env_type = 'prod' if args.prod else 'non_prod'
    auth_creds = auth_token.get_credentials(env_type)
    bearer_token = auth_token.get_auth_token(auth_creds['client_id'], auth_creds['client_secret'], env_type)
    args.bearer_token = bearer_token
    storage_access_info = tenant_storage_access_info_destination.submit_request(args)

    # If just one bucket in storage_access_info, type will be str
    # If more than one bucket in storage_access_info, type(storage_access_info) will be bytes
    if not isinstance(storage_access_info[1], str):
        sys.exit("Got unexpected value from tenant_storage_access_info_destination.submit_request(args). Possibly returned more than one value. Exiting.")
    storage_access_json = json.loads(storage_access_info[1])
    iam_role_arn = storage_access_json[0]['iamRoleArn']
    iam_role_external_id = storage_access_json[0]['iamRoleExternalId']

    ## Create the Boto3 client that represents a live connection to the STS service
    client_a = boto3.client('sts')

    assumed_role_object_a = client_a.assume_role(
        RoleArn = args.dwm_role_arn,
        RoleSessionName = args.dwm_role_session_name,
        DurationSeconds = duration_seconds
    )

    credentials_a = assumed_role_object_a['Credentials']

    # We've now assuemd the WMEA role that has permissions to the customer's S3 bucket. Now need to assume that role
    client_b = boto3.client(
        'sts',
        aws_access_key_id=credentials_a['AccessKeyId'],
        aws_secret_access_key=credentials_a['SecretAccessKey'],
        aws_session_token=credentials_a['SessionToken']
    )

    # Equivalent to the final "aws sts assume-role" command that grants the temporary permissions, e.g.:
    # aws sts assume-role --role-arn=arn:aws:iam::615896027441:role/assetmgmt-wonderland-dmed-aoc \
    #   --external-id=aspera-aoc-14a2412d-fd36-4cca-9cd5-1380a224195e:17df194a-e91a-444d-b23a-f922d23b9ca5 \
    #   --role-session-name=wmea-session
    assumed_role_object_b = client_b.assume_role(
        RoleArn = iam_role_arn,
        ExternalId = iam_role_external_id,
        RoleSessionName = dwm_role_session_name
    )

    assumed_role_credentials = assumed_role_object_b['Credentials']
    # Can't use "os.environ['VARIABLE'] = value" to set the calling shell's environment, because child processess
    # (this one) can't do that. Need to output commands to run from the shell
    os.environ['AWS_ACCESS_KEY_ID'] = assumed_role_credentials['AccessKeyId']
    os.environ['AWS_SECRET_ACCESS_KEY'] = assumed_role_credentials['SecretAccessKey']
    os.environ['AWS_SESSION_TOKEN'] = assumed_role_credentials['SessionToken']
    print("Done assuming customer bucket credentials. Expiry: {0} (UTC)".format(assumed_role_credentials['Expiration'].strftime("%Y-%m-%d %H:%M:%S")))
    print("In order to use aws CLI commands with these credentials, please issue the following commands in your shell:")
    print("export AWS_ACCESS_KEY_ID={0}".format(assumed_role_credentials['AccessKeyId']))
    print("export AWS_SECRET_ACCESS_KEY={0}".format(assumed_role_credentials['SecretAccessKey']))
    print("export AWS_SESSION_TOKEN={0}".format(assumed_role_credentials['SessionToken']))

    sys.exit(0)

if __name__ == '__main__':
    main(sys.argv)
