#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper;

########## Configuration #######################################################
# Fill $title_id with a value if output should only include a specific title id
my $title_id = '';
# Fill $distributor_id with a value if output should only include a specific distributor id
my $distributor_id = '';
my %process = ();
my %completed_process = ();
################################################################################

LINE:
while (<>) {
    chomp;
    if (/^(\w+)\s+(\d+)\s+(\d+:\d+:\d+)\s+(\S+)\s+ascp\[(\d+)\]: LOG Aspera Enterprise Server version/) {
        my $transfer_id = "$4:$5";
        $process{$transfer_id}{start} = "$1 $2 $3";
        $process{$transfer_id}{server} = "$4";
        $process{$transfer_id}{pid} = "$5";

        next LINE;
    }

    if (/^(\w+)\s+(\d+)\s+(\d+:\d+:\d+)\s+(\S+)\s+ascp\[(\d+)\]: LOG Source bytes transferred\s+:\s+(\d+)/) {
        my $transfer_id = "$4:$5";
        $process{$transfer_id}{bytes_transferred} = $6;

        next LINE;
    }
    
    if (/^(\w+)\s+(\d+)\s+(\d+:\d+:\d+)\s+(\S+)\s+ascp\[(\d+)\]: LOG - Source file transfers failed\s+:\s+(\d+)/) {
        my $transfer_id = "$4:$5";
        $process{$transfer_id}{transfers_failed} = $6;

        next LINE;
    }
    
    if (/^(\w+)\s+(\d+)\s+(\d+:\d+:\d+)\s+(\S+)\s+ascp\[(\d+)\]: LOG pvcl:wm path \[([^\]]+)\].+assetID \[([^\]]+)\].+distributorID \[([^\]]+)\].+wprID \[([^\]]+)\].+Asset will be watermarked.+detail \[([^\]]+)\]/) {
        my $transfer_id = "$4:$5";
        $process{$transfer_id}{watermarked} = 'Y';
        $process{$transfer_id}{wm_path} = $6;
        $process{$transfer_id}{asset_id} = $7;
        $process{$transfer_id}{distributor_id} = $8;
        $process{$transfer_id}{title_id} = $9;
        $process{$transfer_id}{wm_reason} = $10;

        next LINE;
    }
    
    if (/^(\w+)\s+(\d+)\s+(\d+:\d+:\d+)\s+(\S+)\s+ascp\[(\d+)\]: LOG pvcl:wm path \[([^\]]+)\].+assetID \[([^\]]+)\].+distributorID \[([^\]]+)\].+wprID \[([^\]]+)\].+Asset will not be watermarked.+detail \[([^\]]+)\]/) {
        my $transfer_id = "$4:$5";
        $process{$transfer_id}{watermarked} = 'N';
        $process{$transfer_id}{wm_path} = $6;
        $process{$transfer_id}{asset_id} = $7;
        $process{$transfer_id}{distributor_id} = $8;
        $process{$transfer_id}{title_id} = $9;
        $process{$transfer_id}{wm_reason} = $10;

        next LINE;
    }
    
    if (/^(\w+)\s+(\d+)\s+(\d+:\d+:\d+)\s+(\S+)\s+ascp\[(\d+)\]: LOG[^:]+: disconnected from DB with spec=/) {
        my $transfer_id = "$4:$5";

        # Skip if end with no start
        next LINE unless exists $process{$transfer_id}{start};

        $process{$transfer_id}{end} = "$1 $2 $3";

        # Make the ID unique, for when PIDs are reused
        my $new_transfer_id = $process{$transfer_id}{start} . ":$transfer_id";
        $completed_process{$new_transfer_id} = delete $process{$transfer_id};

        next LINE;
    }
}

print join("|", map { defined $_ ? $_ : "" } qw(
    pid start end server watermarked wm_path asset_id distributor_id title_id
    wm_reason transfers_failed bytes_transferred
)), "\n";
foreach my $transfer_id (sort keys %completed_process) {
    next unless exists $completed_process{$transfer_id}{start} &&
                exists $completed_process{$transfer_id}{end}   &&
                exists $completed_process{$transfer_id}{title_id};

    if ($title_id) {
        next unless $completed_process{$transfer_id}{title_id} eq $title_id;
    }
    if (exists $completed_process{$transfer_id}{distributor_id} &&
        $distributor_id) {
        next unless lc $completed_process{$transfer_id}{distributor_id} eq
            lc $distributor_id;
    }

    print join("|", map { defined $_ ? $_ : "" } (
        $completed_process{$transfer_id}{pid},
        $completed_process{$transfer_id}{start},
        $completed_process{$transfer_id}{end},
        $completed_process{$transfer_id}{server},
        $completed_process{$transfer_id}{watermarked},
        $completed_process{$transfer_id}{wm_path},
        $completed_process{$transfer_id}{asset_id},
        $completed_process{$transfer_id}{distributor_id},
        $completed_process{$transfer_id}{title_id},
        $completed_process{$transfer_id}{wm_reason},
        $completed_process{$transfer_id}{transfers_failed},
        $completed_process{$transfer_id}{bytes_transferred}
    )), "\n";
}
