#!/usr/bin/perl

use warnings;
use strict;

open my $file, '<', "logan_transfer_ids" or die "Couldn't open: $!";
chomp(my @transfer_ids = <$file>);
close $file or die "Couldn't close: $!";

# Read each line of log and print if it contains one of the wanted transfer IDs
while (<>) {
    foreach my $id (@transfer_ids) {
        (my $id_for_pattern = $id) =~ s/([\[\]])/\\$1/g;
        my $pattern = qr/$id_for_pattern/;
        if ($_ =~ /${pattern}/) {
            print $_;
        } 
    }
}
