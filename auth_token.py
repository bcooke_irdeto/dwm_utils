#!/usr/bin/env python

from argparse import ArgumentParser
from datetime import datetime
import json
import jwt
import requests
import sys
import uuid

env_type = 'non_prod'
audience = 'https://stage.dwm.irdeto.io'
auth0_credentials = {
    "non_prod" : {
        "irdeto" : {
            "client_id" : "gsQkh4FI7cIoKcGKzKQmsDPJAkheVHsa",
            "client_secret" : "V4rUlhc_7AiwE4ZgeRlj3D7kjDyXgNQ7YbfmJeG77qUpNLUvZpN3qKKO8q8ev9Iz"
        },
        "switcher" : {
            "client_id" : "LvveRwOz1czTOSNnm1zW0UFA7DejWpXa",
            "client_secret" : "ippu-5nXMd1Wx1zwhQOYyMzPUpQbM9AEVFzeGB9ij5vOr7EV4MjrxEuIbddIHmP6"
        },
        "cms" : {
            "client_id" : "Dl47wKJwBzTwLF02Fyl3zhtB00iSBEEu",
            "client_secret" : "WzfgEYuQ31m713DfQNbjhKBlVgsQAbtfnoMDLYwL0GIH6zifdl59NhMyqE1iBvou"
        },
        "pacgenesis" : {
            "client_id" : "904e5ff2daa9fd5173bb85f83106a7de5854c1e9@tenants",
            "client_secret" : "38ca99ef37f5228ff562710090e70d877e4779cb9ac12f5d281e2aadd239f9af"
        },
        "sdvi" : {
            "client_id" : "187925e8c6db855c6b0e3e1efb9578465434ffc0@tenants",
            "client_secret" : "184dc86fb3c6e1249fd32043b719d0bc1d7e8f35ed6f7e5ad8d2fce592999e5e"
        },
        "cbsi" : {
            "client_id" : "e84b5c53a58862d0c222d1b928cbb92cb29735dd",
            "client_secret" : "d5a3157c0d9378a6a9d2010efc34fff1250ea72b6b5297a3f336952cc1e18300"
        }
    },
    "prod" : {
        "irdeto" : {
            "client_id" : "2gijIOrCDR9HXGSohLYMB6avVmXZf0wI",
            "client_secret" : "zpvEGeE6rBSTf8kHtepuziCbCqahbPE2NTO6JBe9P2Q8iZVGCohdyBb2pB7Eyg13"
        },
        "switcher" : {
            "client_id" : "ESCjHHKwms9wc8cbs4ukFkgGioi5z9fM",
            "client_secret" : "PyT5YTflVvRrvtiA2r1bnt3fhiSYg2t_nzyOpwgvEs__bM8xWRZD67Er9UImKHnd"
        },
        "cms" : {
            "client_id" : "CVK7Z0agQVyR4jO1GFKrAR6cOaUSRpDw",
            "client_secret" : "V1oAnE8mIWEYgue-FTSidFVMPFhX73KddyzDOMIt2royl9jnf3M4BGI8JMiWOphS"
        },
        "pacgenesis" : {
            "client_id" : "ae9d55b60b4e4df9558123f7bb7d32de802c8570@tenants",
            "client_secret" : "e47fe8e1b18a84847a39a86cc3017c2775017e6acf5b9f20d013d6d4524be8b0"
        },
        "sdvi" : {
            "client_id" : "ae489f8811fa175f8e3f53d3ed76b1c0dc1ec68c@tenants",
            "client_secret" : "7e05e887490c3e1fe556b467a2130ee089db65e862a80a32db0dd5576b533f02"
        },
        "cbsi" : {
            "client_id" : "e84b5c53a58862d0c222d1b928cbb92cb29735dd@tenants",
            "client_secret" : "d5a3157c0d9378a6a9d2010efc34fff1250ea72b6b5297a3f336952cc1e18300"
        }
    }
}
default_credentials = auth0_credentials["non_prod"]["irdeto"]

def main():
    global env_type
    global audience

    parser = ArgumentParser(prog="auth_token.py")
    parser.add_argument("--only-for",
        action="store",
        default="irdeto",
        help="Client alias. Possible values: cms, switcher, irdeto. Default: irdeto"
    )
    parser.add_argument("--prod", action="store_true", default=False, help="Enable production")
    args = parser.parse_args()

    env_type = 'prod' if args.prod else 'non_prod'

    creds = auth0_credentials[env_type][args.only_for]

    access_token   = get_auth_token(creds["client_id"], creds["client_secret"], env_type)
    token_claimset = jwt.decode(access_token, options={"verify_signature": False})
    token_scope    = token_claimset['scope']
    token_expiry   = datetime.utcfromtimestamp(int(token_claimset['exp']))

    print("Irdeto's Token:\n%s" % access_token)
    print("Scope: %s" % token_scope)
    print("Expires: %s" % token_expiry)

def get_credentials(e='non_prod'):
    env_type = e
    credentials = auth0_credentials[env_type]["irdeto"]
    return credentials

def get_token_info(client_id, auth_secret, e):
    global audience
    env_type = e

    #url = "https://irdetodev.auth0.com/oauth/token"
    url = "https://irdeto.auth0.com/oauth/token"
    #url = "https://us-east-1.prod.dwm.irdeto.io/oauth/token"   # Use for requesting a token using individual customer credentials
    audience = 'https://prod.dwm.irdeto.io' if env_type == 'prod' else 'https://stage.dwm.irdeto.io'
    payload = dict(
        grant_type="client_credentials",
        client_id=client_id,
        client_secret=auth_secret,
        audience=audience
    )
    headers = {
        'content-type': "application/json"
    }

    response = requests.post(url=url, headers=headers, json=payload)

    if response.status_code == 200:
        d = response.json()
        return d
    elif response.status_code < 200 or response.status_code > 200:
        print("Error. Non 200 response got: %s.\n" % (response.status_code))
        d = response.json()
        print(json.dumps(d, indent=4))
        sys.exit(1)

def get_auth_token(client_id=default_credentials["client_id"], auth_secret=default_credentials["client_secret"], e="non_prod"):
    access_token = ''
    env_type = e
    token_filename = 'access_token_' + env_type

    try:
        access_token = open(token_filename, 'r').read()
    except IOError as e:
        print("Could not open the access token file: {0}".format(e))
        ird = get_token_info(client_id, auth_secret, env_type)
        access_token = ird['access_token']
        token_claimset = jwt.decode(access_token, options={"verify_signature": False})
        with open(token_filename, 'w') as f:
            f.write(access_token)

    token_claimset = jwt.decode(access_token, options={"verify_signature": False})
    token_expiry = datetime.utcfromtimestamp(int(token_claimset['exp']))
    now = datetime.utcnow()

    if token_expiry <= now:
        print("Token expired at {0}. Requesting new token.".format(token_expiry))
        ird = get_token_info(client_id, auth_secret, env_type)
        access_token = ird['access_token']
        token_claimset = jwt.decode(access_token, options={"verify_signature": False})
        with open(token_filename, 'w') as f:
            f.write(access_token)

    return access_token

if __name__ == '__main__':main()
