#!/usr/bin/env python

import json
#from pprint import pprint

jobs_file_path = '/home/bcooke/transfer_jobs_asof_20170306.js'

with open(jobs_file_path) as jobs_file:
    data = json.load(jobs_file)
    jobs = data['transfers']

asset_ids = [
  '5436629',
  '5436631',
  '5436630',
  '5438902',
  '5438973',
  '5441313',
  '5441312',
  '5441834',
  '5441848',
  '5442286',
  '5442368',
  '5445145',
  '5445146',
  '5445345',
  '5445344',
  '5445477',
  '5505780',
  '5555223',
  '5556573',
  '5556558',
  '5562583',
  '5562626',
  '5576389',
  '5576388',
  '5492410',
  '5448382',
  '5586011',
  '5586186',
  '5588104',
  '5588344',
  '5599587',
  '5485023',
  '5601555',
  '5601897',
  '5601898',
  '5603714',
  '5604055',
  '5604378',
  '5604841',
  '5605419',
  '5605443',
  '5605493',
  '5605500',
  '5605501',
  '5605502',
  '5606065',
  '5606167',
  '5606921',
  '5607937',
  '5608009',
  '5609818',
  '5612020',
  '5612019',
  '5612055',
  '5612094',
  '5615289',
  '5615701',
  '5616333',
  '5616581',
  '5616836',
  '5624185',
  '5625244',
  '5625312',
  '5630470',
  '5630472',
  '5630510',
  '5631106',
  '5633515',
  '5606745',
  '5655190',
  '5655205',
  '5655959',
  '5669393',
  '5671145',
  '5612095',
  '5694248',
  '5704783',
  '5704784',
  '5705353',
  '5708265',
  '5710007',
  '5710719',
  '5710873',
  '5711612',
  '5764607',
  '5765773'
]

for job in jobs:
    if 'completionTime' not in job:
        continue

    if job['error']['code'] != 0:
        continue

    if job['assetId'] not in asset_ids:
        continue

    print "\t".join([
        str(job['transferId']), job['assetId'], job['distributorId'],
        job['inFilePath'], job['startTime'], job['completionTime']
    ]) 

#    print "\t".join([
#        str(job['jobId']), job['inFilePath'], job['titleId'], job['assetId'],
#        job['startTime'], job['completionTime']
#    ])
