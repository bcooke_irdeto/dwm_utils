#!/usr/bin/env python

import json
#from pprint import pprint

jobs_file_path = '/home/bcooke/wm_jobs_asof_20170309.js'

with open(jobs_file_path) as jobs_file:
    data = json.load(jobs_file)
    jobs = data['jobs']

#pprint(data)
#pprint(jobs)

print "\t".join([
"job_id", "in_file_path", "title_id", "asset_id",
"error_code", "error_message",
"submission_time", "start_time", "completion_time"
])
for job in jobs:
    if job['status'] != "ERROR":
        continue
    print "\t".join([
        str(job['jobId']), job['inFilePath'], job['titleId'], job['assetId'],
        str(job['error']['code']), job['error']['message'],
        job['submissionTime'], job.get('startTime', ''), job.get('completionTime', '')
    ])
