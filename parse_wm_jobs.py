#!/usr/bin/env python

import json
#from pprint import pprint

jobs_file_path = '/home/bcooke/wm_jobs_asof_20170505.js'

with open(jobs_file_path) as jobs_file:
    data = json.load(jobs_file)
    jobs = data['jobs']

#pprint(data)
#pprint(jobs)

for job in jobs:
    if job['status'] != "COMPLETE":
        continue
    print "\t".join([
        str(job['jobId']), job['inFilePath'], job['titleId'], job['assetId'],
        job['submissionTime'], job['startTime'], job['completionTime']
    ])
