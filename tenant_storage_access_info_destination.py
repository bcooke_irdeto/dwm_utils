#!/usr/bin/env python

import sys
import argparse
import json
import urllib.request
import urllib.error
import http.client
import ssl
import auth_token

def parse_args():
    parser = argparse.ArgumentParser(description="Return a JSON string containing information about a DWM tenant's storage access configuration.")
    parser.add_argument('--host', help='DWM front-end host name or IP (default: us-east-1.stage.dwm.irdeto.io)', default='us-east-1.stage.dwm.irdeto.io')
    parser.add_argument('--port', help='DWM front-end port number (default: 443)', default='443')
    parser.add_argument('-b', '--bucket_name', help="Name of the customer's S3 storage bucket")
    parser.add_argument('--ignore-ssl-cert', help="Don't verify SSL certificates. NOT to be used in production.", action='store_true')
    parser.add_argument('--prod', action="store_true", default=False, help='Enable production mode, which will request auth token for prod systems. Use this when the --host parameter is for a production host.')
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument('-i', '--tenant_id', help="Customer's tenant ID number.", required=True)
    args = parser.parse_args()

    return args

def prettify(text):
    try:
        json_content = json.loads(text)
        json_pp = json.dumps(json_content, sort_keys=True, indent=4, separators=(',', ': '))
        return json_pp
    except:
        return text 

def submit_request(args):
    uri = 'https://' + args.host + ':' + args.port + '/tenants/' + args.tenant_id + '/configuration/aws/storage-access'
    params = {}
    if hasattr(args, 'bucket_name') and args.bucket_name:
        params['s3BaseUri'] = args.bucket_name
    params_for_uri = urllib.parse.urlencode(params)
    if params:
        uri = "{0}?{1}".format(uri, params_for_uri)

    try:
        return_code = 0
        json_str    = ""
       
        if args.ignore_ssl_cert:
            ssl._create_default_https_context = ssl._create_unverified_context

        req = urllib.request.Request(uri)

        #if bearer_token:
        if args.bearer_token:
            req.add_header('Authorization', "Bearer " + args.bearer_token)

        res = urllib.request.urlopen(req)

        json_str = res.read()

        # The API doesn't properly filter on the published s3BaseUri parameter. Handle that filtering here.
        if json_str:
            if hasattr(args, 'bucket_name') and args.bucket_name:
                json_obj = json.loads(json_str)
                destinations = [destination for destination in json_obj['destinationsAccessConfig'] if args.bucket_name in destination['s3UriBucketBase']]
                json_str = json.dumps(destinations)

        if res.getcode() == 200:
            return_code = 0
        else:
            return_code = 70
    except urllib.error.HTTPError as e:
        print("HTTPError: {0}".format(str(e.code)))
        print(e.read())
        return_code = 70
    except urllib.error.URLError as e:
        print("URLError: " + str(e.reason))
        return_code = 70
    except http.client.HTTPException as e:
        print( "HTTPException")
        print(e)
        return_code = 70
    except Exception:
        import traceback
        print("Encountered generic exception: " + traceback.format_exc())
        return_code = 70
    finally:
        return (return_code, json_str)

def main(argv):
    #global bearer_token

    args = parse_args()

    env_type = 'prod' if args.prod else 'non_prod'
    auth_creds = auth_token.get_credentials(env_type)
    bearer_token = auth_token.get_auth_token(auth_creds['client_id'], auth_creds['client_secret'], env_type)
    args.bearer_token = bearer_token

    (return_code, json_str) = submit_request(args)

    print(prettify(json_str))

    sys.exit(return_code)

if __name__ == '__main__':
    main(sys.argv)
